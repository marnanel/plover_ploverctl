# ploverctl

External control for
[Plover](https://github.com/openstenoproject/plover).

# How to Use

After installing, enable the plugin in the Plover Configure -> Plugins
menu.

Then, enable Plover with `ploverctl on`, disable Plover with `ploverctl off`,
or toggle output with `ploverctl toggle`.

# Acknowledgements

The code is heavily based on *Plover Dictionary Lookup* by
Nick Markopoulos <nsmarkop@gmail.com>.
