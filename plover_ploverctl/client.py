import argparse
import xdg
import os

def main():
    parser = argparse.ArgumentParser(
            description='send controls to Plover')
    parser.add_argument(
            'command', 
            type=str,
            choices=['on','off', 'toggle'],
            help='message to send to Plover')
    args = parser.parse_args()

    try:
        filename = os.path.join(
                xdg.XDG_CONFIG_HOME,
                'plover',
                'ploverctl',
                )

        with open(filename, 'w') as fifo:
            fifo.write(args.command+'\r\n')
    except FileNotFoundError:
        print('Either Plover is not running, or the plover_ploverctl plugin '+\
                'isn\'t enabled in the settings.')

if __name__ == "__main__":
    main()
