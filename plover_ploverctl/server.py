import os
import xdg
from threading import Event, Thread
from plover import log
from plover.engine import StenoEngine

LOG_PREFIX = "plover_ploverctl:"

class ControlServer(Thread):

    def __init__(self, engine: StenoEngine):
        super().__init__()
        log.info(f"{LOG_PREFIX} initializing")

        self._engine = engine
        self._running = False

    def start(self):
        log.info(f"{LOG_PREFIX} starting")

        pathname = os.path.join(
                xdg.XDG_CONFIG_HOME,
                'plover',
                )
        try:
            os.makedirs(pathname)
        except FileExistsError:
            pass

        self._filename = os.path.join(
                pathname,
                'ploverctl',
                )

        try:
            os.mkfifo(self._filename)
        except FileExistsError:
            pass

        self._running = True
        super().start()

    def stop(self):
        log.info(f"{LOG_PREFIX} stopping")
        self._running = False
        try:
            with open(self._filename, 'w') as fifo:
                fifo.write('\r\n')

            os.unlink(self._filename)
        except os.FileNotFoundError:
            pass

    def run(self):
        """Main server loop."""

        log.debug(f'{LOG_PREFIX} - start main loop')

        while self._running:
            with open(self._filename, 'r') as fifo:
                command = fifo.read().strip()

            if command=='on':
                log.debug(f'{LOG_PREFIX} - start output')
                self._engine.set_output(True)
            elif command=='off':
                log.debug(f'{LOG_PREFIX} - stop output')
                self._engine.set_output(False)
            elif command=='toggle':
                log.debug(f'{LOG_PREFIX} - toggle output')
                self._engine.toggle_output()
            else:
                log.info(f'{LOG_PREFIX} - unknown command "{command}"')
